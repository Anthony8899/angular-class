import { Component } from '@angular/core';
import { Employee } from './Employee';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  model = new Employee("", "", "");

  onSubmit(test:any) {
    console.log(this.model);
  }

  naming(name:any){
    console.log(name);
  }
}
