import { Address } from "./address.model";


export interface Student {
    name: string;
    age: string;
    salary: string;
    address: Address;
}
