import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Student } from './student.model';

@Component({
  selector: 'app-practice',
  templateUrl: './practice.component.html',
  styleUrls: ['./practice.component.css']
})
export class PracticeComponent{

  constructor(private fb: FormBuilder){

  }

  student = this.fb.group({
    name : [''],
    age : [''],
    salary : [''],
    address : this.fb.group({
      dno : [''],
      street : ['']
    })
  });
  
  onSubmit(){
    console.log(this.student.value);
  }

  onUpdate(){
    this.student.patchValue({
      name : "Testing",
      address : {
        street : "updating street"
      }
    });
  }

}
