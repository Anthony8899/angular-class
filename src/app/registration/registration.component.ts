import { Component} from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent{

  student = new FormGroup({
    sname : new FormControl("Tony"),
    age : new FormControl("26"),
    email : new FormControl("abc@gmail.com"),
    pass : new FormControl("12345"),
    college : new FormControl("sjc"),
    gender : new FormControl("female"),
    dob : new FormControl(new Date()),
    address : new FormControl("Testing address")
  });

  onSubmit(){
    console.log(this.student.value);
  }

}
