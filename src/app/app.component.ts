import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title(title: any) {
    throw new Error('Method not implemented.');
  }
  name: string = "Tesing";
  age: number = 0;
  status: boolean = false;
  message: string = "Hi Hello How are you??!!!!";
  servers: string[] = [];
  server: string = "";
  serverStatus: boolean = false;

  constructor() {
    setTimeout(() => {
      this.status = true;
    }, 2000);
  }

  messages(){
    console.log("Coming");
    return this.message;
  }

  onClick(){
    this.serverStatus = true;
    this.servers.push(this.server)
  }

  nameing(event: any){
    console.log(event.target.value);
    this.name = event.target.value;
  }

  ageing(event: any){
    console.log(event.target.value);
    this.age = event.target.value;
  }

}
